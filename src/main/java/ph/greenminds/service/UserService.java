package ph.greenminds.service;

import java.util.Date;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ph.greenminds.model.Farmer;
import ph.greenminds.model.PoolEntity;
import ph.greenminds.model.User;
import ph.greenminds.repository.FarmerRepository;
import ph.greenminds.repository.PoolEntityRepository;
import ph.greenminds.repository.UserRepository;
import ph.greenminds.service.AuthorizationService.BtrcRole;

@Service
public class UserService {
	
	@Inject
	private PoolEntityRepository poolEntityRepository;
	
	@Inject
	private UserRepository userRepository;
	
	@Inject
	private AuthorizationService authService;
	
	@Inject
	private FarmerRepository farmerRepository;
	
	public void saveAreaManager(long poolEntityId, String fullname) {
		PoolEntity poolEntity = poolEntityRepository.findOne(poolEntityId);
		if (poolEntity == null) {
			throw new RuntimeException("No pool entity found: "+poolEntityId);
		}
		
		User user = userRepository.findByFullname(fullname);
		if (user == null) {
			user = new User();
			user.username = new Date().getTime()+"";
			user.fullname= fullname;
			
		}
		user.roles.add(authService.getAreaManagerRole());
		user.poolEntity = poolEntity;
		user.type = BtrcRole.AREA_MANAGER.name();
		userRepository.save(user);
		
		
	}
	
	public void saveFarmSupervisor(long poolEntityId, String fullname) {
		PoolEntity poolEntity = poolEntityRepository.findOne(poolEntityId);
		if (poolEntity == null) {
			throw new RuntimeException("No pool entity found: "+poolEntityId);
		}

		User user = userRepository.findByFullname(fullname);
		if (user == null) {
			user = new User();
			user.username = new Date().getTime()+"";
			user.fullname= fullname;
			
		}
		user.roles.add(authService.getAreaManagerRole());
		user.poolEntity = poolEntity;
		user.type = BtrcRole.FARM_SUPERVISOR.name();
		userRepository.save(user);
		
		
	}
	
	public void saveFarmer(long poolEntityId, String fullname, String address, String city, String area, Long farmArea) {
		PoolEntity poolEntity = poolEntityRepository.findOne(poolEntityId);
		if (poolEntity == null) {
			throw new RuntimeException("No pool entity found: "+poolEntityId);
		}

		User user = userRepository.findByFullname(fullname);
		Farmer farmer = new Farmer();
		if (user == null) {
			user = new User();
			user.username = new Date().getTime()+"";
			userRepository.save(user);
			farmer.user = user;
			farmerRepository.save(farmer);
		} else {
			farmer = farmerRepository.findByUserId(user.id);
		}
		if(!user.roles.contains(authService.getAreaManagerRole()))
		user.roles.add(authService.getAreaManagerRole());

		user.fullname= fullname;
		user.poolEntity = poolEntity;
		user.type = BtrcRole.FARMER.name();
		user.address = address;
		user.city = city;
		userRepository.save(user);
		farmer.area = area;
		farmer.farmArea = farmArea;
		farmerRepository.save(farmer);
		
		
		
		
		
	}
	
	
}
