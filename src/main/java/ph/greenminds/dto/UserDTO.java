package ph.greenminds.dto;

public class UserDTO {

	public Long id;
	
	public String name;
	
	public String address;
	
	public String city;
	
	public String area;
	
	public Double farmArea;
	
}
