package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.CropAssignment;

public interface CropAssignmentRepository extends JpaRepository<CropAssignment, Long>{

}
