package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Long>{

}
