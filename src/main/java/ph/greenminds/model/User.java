package ph.greenminds.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * 
 * @author mahvine
 * @since Feb 4, 2018
 *
 */
@Data
@javax.persistence.Entity
@Table(name="c_user")
public class User {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long id;
	
	@Column(name="username",unique=true, length=50)
	public String username;
	
	public String fullname;
	
	public String address;
	
	public String city;

	public String type;
	
	public String area;
	
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="pool_entity_id", referencedColumnName="id")
	public PoolEntity poolEntity;
	
	
	@JsonIgnore
	@ManyToMany
    @JoinTable(name = "c_user_roles",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "roles_id"))
	public List<Role> roles = new ArrayList<Role>();

}
