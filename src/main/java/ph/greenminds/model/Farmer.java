package ph.greenminds.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @author mahvine
 * @since Feb 4, 2018
 *
 */
@Data
@javax.persistence.Entity
@Table(name="c_farmer")
public class Farmer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@OneToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	public Long farmArea;
	
	public String area;

}
