package ph.greenminds.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Also similar to a Company
 * @author mahvine
 * @since Feb 12, 2018
 *
 */
@Data
@javax.persistence.Entity
@Table(name="c_pool_entity")
public class PoolEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String name;
	
	public String description;
	
	public String address;
	
	public String contactNumber;

}
