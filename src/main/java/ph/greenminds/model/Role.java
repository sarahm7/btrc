package ph.greenminds.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @author mahvine
 * @since Feb 4, 2018
 *
 */
@Data
@javax.persistence.Entity
@Table(name="c_role")
public class Role {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@Column(name="name",unique=true, length=100)
	public String name;
	
	@ManyToMany
    @JoinTable(name = "c_role_permissions",
        joinColumns = @JoinColumn(name = "role_id"),
        inverseJoinColumns = @JoinColumn(name = "permission_id"))
	public List<Permission> permissions = new ArrayList<Permission>();

}
