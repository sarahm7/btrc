package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.model.PoolEntity;
import ph.greenminds.repository.PoolEntityRepository;
import ph.greenminds.service.UserService;
import ph.greenminds.web.rest.util.PaginationUtil;

/**
 * 
 * @author mahvine
 * @since Feb 12, 2018
 *
 */
@RestController
@RequestMapping("/api/entities")
public class PoolEntityResource {

	private static final Logger logger = LoggerFactory.getLogger(PoolEntityResource.class);

	@Inject
	PoolEntityRepository poolEntityRepository;
	
	@Inject
	UserService userService;

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody PoolEntity poolEntity, HttpServletRequest request) {
		poolEntityRepository.save(poolEntity);

		logger.info("Saved poolEntity:{}",poolEntity);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<PoolEntity>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<PoolEntity> pageResult = poolEntityRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities", page, size);
		return new ResponseEntity<List<PoolEntity>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{entityId}", method = RequestMethod.GET)
	public PoolEntity getById(@PathVariable("entityId") Long id) {
		return poolEntityRepository.findOne(id);
	}
	

	@RequestMapping(path = "/{entityId}/areaManagers",method = RequestMethod.POST)
	public void saveAreaManager(@PathVariable("entityId") Long entityId, HttpServletRequest request) {
		PoolEntity poolEntity = poolEntityRepository.findOne(entityId);
		logger.info("Saved poolEntity:{}",poolEntity);
	}
	

	@RequestMapping(path = "/{entityId}/farmSupervisors",method = RequestMethod.POST)
	public void saveFarmSupervisor(@PathVariable("entityId") Long entityId, HttpServletRequest request) {
		PoolEntity poolEntity = poolEntityRepository.findOne(entityId);

		logger.info("Saved poolEntity:{}",poolEntity);
	}
	
	@RequestMapping(path = "/{entityId}/farmers",method = RequestMethod.POST)
	public void saveFarmers(@PathVariable("entityId") Long entityId, HttpServletRequest request) {
		PoolEntity poolEntity = poolEntityRepository.findOne(entityId);
		
		logger.info("Saved poolEntity:{}",poolEntity);
	}

}
