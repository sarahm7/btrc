package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.model.Crop;
import ph.greenminds.model.User;
import ph.greenminds.repository.CropRepository;
import ph.greenminds.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api/crops")
public class CropResource {

	private static final Logger logger = LoggerFactory.getLogger(CropResource.class);

	@Inject
	CropRepository cropRepository;

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody Crop crop, HttpServletRequest request) {
		cropRepository.save(crop);
		logger.info("Saved crop:{}",crop);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Crop>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<Crop> pageResult = cropRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities", page, size);
		return new ResponseEntity<List<Crop>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{cropId}", method = RequestMethod.GET)
	public Crop getById(@PathVariable("cropId") Long id) {
		return cropRepository.findOne(id);
	}

}
