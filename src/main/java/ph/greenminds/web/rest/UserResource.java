package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.model.User;
import ph.greenminds.repository.UserRepository;
import ph.greenminds.web.rest.util.PaginationUtil;

/**
 * 
 * @author mahvine
 * @since Feb 4, 2018
 *
 */
@RestController
@RequestMapping("/api/users")
public class UserResource {
	
	private static final Logger logger = LoggerFactory.getLogger(UserResource.class);
	

	@Inject
	UserRepository userRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public void save(@Valid @RequestBody User user,HttpServletRequest request){
		userRepository.save(user);
		logger.info("Saved user:{}",user);
	}
	

	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<User>> listUsers(@RequestParam(value="page",required=false, defaultValue="1") int page,
			@RequestParam(value="size",required=false, defaultValue="10") int size, @RequestParam(value="sort", defaultValue="id") String sortBy,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction) throws URISyntaxException{
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<User> pageUsers = userRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageUsers, "/api/users", page, size);
		return new ResponseEntity<List<User>>(pageUsers.getContent(), headers, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(path="/{userId}",method=RequestMethod.GET)
	public User getById(@PathVariable("userId") Long id){
		return userRepository.findOne(id);
	}


}
