package ph.greenminds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author mahvine
 *
 * @since Feb 4, 2018
 */
@SpringBootApplication
public class BtrcApplication {

	public static final String DATE_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ssZZZZZ";
	public static void main(String[] args) {
		SpringApplication.run(BtrcApplication.class, args);
	}
}
