import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-entity-list',
  templateUrl: './entity-list.component.html',
  styleUrls: ['./entity-list.component.css']
})
export class EntityListComponent implements OnInit {

  data: Array<Entity>;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    //TODO this should be transferrred to a service
    this.http.get<Entity[]>('/api/entities').subscribe(response => {
      this.data = response;
    });
  }

}

export class Entity {
  name: String;
  description: String;
  address: String;
  contactNumber: String;
}
