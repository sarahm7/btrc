import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../../components/components.module';
import { HomeRoutingModule } from './home.routing';

import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { EntityListComponent } from './entities/entity-list.component';
import { ManagerListComponent } from './area-manager/manager-list.component';
import { FarmerListComponent } from './farmers/farmer-list.component';
import { CropListComponent } from './crops/crop-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';

import { HttpClientModule } from '@angular/common/http';
import { CropListService } from './crops/crop-list.service';
import { AddCropsComponent } from './crops/add-crops/add-crops.component';
import { AddCropsService } from './crops/add-crops/add-crops.service';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ComponentsModule,
    HomeRoutingModule,
    RouterModule,
    HttpModule
  ],
  declarations: [
    HomeComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    EntityListComponent,
    ManagerListComponent,
    FarmerListComponent,
    CropListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent, AddCropsComponent,
  ],
  providers: [
    CropListService,
    AddCropsService
  ]
})
export class HomeModule { }
