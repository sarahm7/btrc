import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farmer-list',
  templateUrl: './farmer-list.component.html',
  styleUrls: ['./farmer-list.component.css']
})
export class FarmerListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

export class Farmer {
  name: String;
  address: String;
  city: String;
  farmArea: Number;
  area: String;
}
