import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manager-list',
  templateUrl: './manager-list.component.html',
  styleUrls: ['./manager-list.component.css']
})
export class ManagerListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

export class Manager {
  name: String;
  description: String;
  address: String;
  contactNumber: String;
}
