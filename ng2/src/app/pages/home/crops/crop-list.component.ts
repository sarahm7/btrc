import { Component, OnInit, EventEmitter } from '@angular/core';
import { Crops } from './crop-list.model';
import { CropListService } from './crop-list.service';

@Component({
  selector: 'app-crop-list',
  templateUrl: './crop-list.component.html',
  styleUrls: ['./crop-list.component.css']
})
export class CropListComponent implements OnInit {
  crops: Crops[] = [];
  selectedCrop: Crops;
  inEditMode: boolean;

  constructor(private service: CropListService) { }

  ngOnInit(): void {
    this.reset();
  }

  public select(crop: Crops): void {
    this.getCropById(crop.id);
  }

  private getCrops(): void {
    this.service.all().subscribe((response) => {
      this.crops = response;
    });
  }

  public cropChange(event): void {
    this.reset();
  }

  private getCropById(id: number) {
    this.service.get(id).subscribe((response) => {
      this.selectedCrop = response;
      this.inEditMode = true;
    });
  }

  private reset() {
    this.selectedCrop = this.service.reset();
    this.getCrops();
    this.inEditMode = false;
  }
}
