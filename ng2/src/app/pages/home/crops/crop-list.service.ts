import { Injectable } from '@angular/core';
import { Crops } from './crop-list.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CropListService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  all(): Observable<Crops[]> {
    return this.http.get<Crops[]>(this.apiUrl + 'crops').map((response) => response);
  }

  get(id: number): Observable<Crops> {
    return this.http.get<Crops>(this.apiUrl + `crops/${id}`).map((response) => response);
  }

  reset(): Crops {
    return {
      yieldPerSqm: 0,
      sharePerSold: 0,
      plantingToHarvestDuration: 0,
      name: '',
      sellingPrice: 0,
      id: 0,
      plantingToHarvestUnits: '',
      volumeUnits: ''
    };
  }
}
