import { TestBed, inject } from '@angular/core/testing';

import { CropListService } from './crop-list.service';

describe('CropListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CropListService]
    });
  });

  it('should be created', inject([CropListService], (service: CropListService) => {
    expect(service).toBeTruthy();
  }));
});
