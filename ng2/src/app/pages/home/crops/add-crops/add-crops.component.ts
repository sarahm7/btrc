import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Crops } from '../crop-list.model';
import { AddCropsService } from './add-crops.service';
import { CropListService } from '../crop-list.service';

@Component({
  selector: 'app-add-crops',
  templateUrl: './add-crops.component.html',
  styleUrls: ['./add-crops.component.css']
})
export class AddCropsComponent implements OnInit {
  @Input() crop: Crops;
  @Input() inEditMode: boolean;
  @Output() cropChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(
    private service: AddCropsService,
    private baseService: CropListService
  ) { }

  ngOnInit() {
    this.reset();
  }

  public submit(crop: Crops): void {
    this.service.post(crop).subscribe((response) => {
      this.reset();
      this.cropChange.emit(true);
    })
  }

  public reset(): void {
    this.crop = this.baseService.reset();
    this.inEditMode = false;
  }
}
