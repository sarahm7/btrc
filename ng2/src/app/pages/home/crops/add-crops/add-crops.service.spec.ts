import { TestBed, inject } from '@angular/core/testing';

import { AddCropsService } from './add-crops.service';

describe('AddCropsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddCropsService]
    });
  });

  it('should be created', inject([AddCropsService], (service: AddCropsService) => {
    expect(service).toBeTruthy();
  }));
});
