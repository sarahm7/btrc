import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Crops } from '../crop-list.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AddCropsService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  post(crop: Crops): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http.post(this.apiUrl + 'crops', crop, { headers: headers }).map((response) => response);
  }
}
