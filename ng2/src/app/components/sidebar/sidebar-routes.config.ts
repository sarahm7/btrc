import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    { path: 'entities', title: 'Entities', icon: 'account_balance', class: '' },
    { path: 'area-manager', title: 'Area Manager', icon: 'person', class: '' },
    { path: 'farmers', title: 'Farmers', icon: 'directions_walk', class: '' },
    { path: 'crops', title: 'Crops', icon: 'local_florist', class: '' }
];
